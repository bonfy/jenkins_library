#!groovy

def log(String typ="INFO", String message = "This is a Message") {
    echo "${typ} : ${message}"
}

def info(String message = "This is a Message") {
    log("INFO", message)
}

def warn(String message = "This is a Message") {
    log("WARN", message)
}